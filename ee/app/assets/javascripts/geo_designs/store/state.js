const createState = () => ({
  isLoading: false,

  designs: [],
  totalDesigns: 0,
  pageSize: 0,
  currentPage: 1,
});
export default createState;
